function initPopup() {
    $('.resource__container .resource').each(function(index, el) {
        var popup_type = $(this).find('.resource-for-popup').data('popup-type');

        if(popup_type === 'image') {
            console.log(popup_type);
            var image_src = $(this).find('.resource-for-popup span.outer-html').text();
            $(this).find('a').magnificPopup({
                items: {
                    src: image_src
                },
                type: 'image',
            });
        }
        else if (popup_type === 'video') {
            console.log(popup_type);
            var video_src = $(this).find('.resource-for-popup span.outer-html').text();
            var html_code = '<div class="video-popup-wrapper" style="display: block;"><video class="video-popup" controls=""><source src="' + video_src + '" type="video/mp4"></video></div>';

            $(this).find('a').magnificPopup({
              items: {
                  src: html_code
              },
              type: 'inline'
            });
        }
        else if (popup_type === 'form') {
            console.log(popup_type);
            var form_html = $(this).find('.resource-for-popup span.outer-html').html();
            $(this).find('a').magnificPopup({
              items: {
                  src: form_html
              },
              type: 'inline'
            });
        }
    });
}

$(document).ready(function() {
    $('.resource a').click(function(event) {
        event.preventDefault();
    });
});

$( window ).load(function() {
    initPopup();
    $('.resource__container .resource').each(function(){
        if($(this).find('a.cta_button').text() === "DOWNLOAD") {
            $(this).find('.resource__image-container a').click();
        }
    });
});
