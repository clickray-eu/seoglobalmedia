( function ($) {
	$.fn.cutWrappers = function(targetSelector) {
		$(this).find(targetSelector).appendTo($(this));
		$(this).find('.hs_cos_wrapper_type_custom_widget').remove();
		$(this).find('.hs_cos_wrapper_type_widget_container').remove();
		$(this).find('.widget-span').remove();
}}(jQuery));

//$(".pricing-box__container").cutWrappers('.pricing-box');