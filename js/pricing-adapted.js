$(document).ready(function() {
	$('.pricing-box--redesign .pricing-box__more-features-button').on('click', function() {
		if ($(this).parent().hasClass('open')) {
			$(this).parent().removeClass('open');
		} else {
			$('.pricing-box--redesign .pricing-box__more-features').removeClass('open');
			$(this).parent().addClass('open');
		}
	})
})